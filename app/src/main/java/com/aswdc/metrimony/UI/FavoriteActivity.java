package com.aswdc.metrimony.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.TextView;

import com.aswdc.metrimony.Adapter.CandidateListAdapter;
import com.aswdc.metrimony.Database.TblCandidate;
import com.aswdc.metrimony.Model.CandidateModel;
import com.aswdc.metrimony.R;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class FavoriteActivity extends AppCompatActivity {

    //Candidate ArrayList
    ArrayList<CandidateModel> candidateList ;

    //TblCandidate Object
    TblCandidate tblCandidate;

    //CandidateList Adepter Object
    CandidateListAdapter adapter;


    //Textview object
    TextView activity_favorite_tvNoDataFound;

    //RecylerView Object
    RecyclerView activity_favorite_rvFavoriteList;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite);



        init();
        process();
        listener();


    }

    private void init() {

        //Candidate Array List Empty
        candidateList = new ArrayList<>();

        //TblCandidate  Initialize
        tblCandidate = new TblCandidate(this);

        //get data from database and set into Array list
        candidateList = tblCandidate.all_CandidateFavoriteList();

        //TextView Initialize
        activity_favorite_tvNoDataFound = findViewById(R.id.activity_favorite_tvNoDataFound);

        //RecylerView Initialize
        activity_favorite_rvFavoriteList = findViewById(R.id.activity_favorite_rvFavoriteList);

    }

    private void process() {

        if (candidateList.size()<1){
            activity_favorite_tvNoDataFound.setVisibility(View.VISIBLE);
            activity_favorite_rvFavoriteList.setVisibility(View.GONE);
        }else {


            activity_favorite_tvNoDataFound.setVisibility(View.GONE);
            activity_favorite_rvFavoriteList.setVisibility(View.VISIBLE);



            //Recycler View Layout Manager set
            activity_favorite_rvFavoriteList.setLayoutManager(new GridLayoutManager(this,1));

            //Adepter Set
            adapter = new CandidateListAdapter(candidateList,this);

            //Recycker View set Adapter
            activity_favorite_rvFavoriteList.setAdapter(adapter);

        }


    }

    private void listener() {
    }
}