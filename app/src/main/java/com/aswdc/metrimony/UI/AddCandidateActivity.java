package com.aswdc.metrimony.UI;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;


import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.aswdc.metrimony.Adapter.SpCityAdapter;
import com.aswdc.metrimony.Adapter.SpLanguageAdapter;
import com.aswdc.metrimony.Database.TblCandidate;
import com.aswdc.metrimony.Database.TblCity;
import com.aswdc.metrimony.Database.TblLanguage;
import com.aswdc.metrimony.Model.CandidateModel;
import com.aswdc.metrimony.Model.CityModel;
import com.aswdc.metrimony.Model.LanguageModel;
import com.aswdc.metrimony.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class AddCandidateActivity extends AppCompatActivity {


    //Edit Text Object for get user INput
    EditText activity_add_candidate_etName, activity_add_candidate_etFatherName, activity_add_candidate_etSurName, activity_add_candidate_etDOB, activity_add_candidate_etMObile, activity_add_candidate_etHobbies, activity_add_candidate_etEmail;

    //Radio Button for Gender

    RadioButton activity_add_candidate_rbMale, activity_add_candidate_rbFemale;

    // Spinner for select citu or langvuege

    Spinner activity_add_candidate_spCity, activity_add_candidate_spLanguage;

    // Butoon for user

    Button activity_add_candidate_btnSubmit;

    //create canditade model object
    CandidateModel candidateModel;

    // Tbl CAndidate Object
    TblCandidate tblCandidate;

    //Tbl City Object
    TblCity tblCity;

    //Tbl Language Object
    TblLanguage tblLanguage;

    //ArrayList
    ArrayList<CityModel> cityList;
    ArrayList<LanguageModel> languageList;

    //Adapter Object
    SpCityAdapter spCityAdapter;
    SpLanguageAdapter spLanguageAdapter;


    //check inserted sucessfully
    long insertedCheck;

    //Tamp variable for get date form user
    String name, fatherName, surName, DOB, email, mobile, hobbies;
    int spCityPostion, spLanguagePostion;
    boolean rbMale;


    //Calander Instance
    Calendar calendar;

    //Calender Dialog
    DatePickerDialog.OnDateSetListener date;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_candidate);


        init();
        process();
        listeners();

    }

    private void init() {

        //Edittext
        activity_add_candidate_etName = findViewById(R.id.activity_add_candidate_etName);
        activity_add_candidate_etFatherName = findViewById(R.id.activity_add_candidate_etFatherName);
        activity_add_candidate_etSurName = findViewById(R.id.activity_add_candidate_etSurName);
        activity_add_candidate_etDOB = findViewById(R.id.activity_add_candidate_etDOB);
        activity_add_candidate_etMObile = findViewById(R.id.activity_add_candidate_etMObile);
        activity_add_candidate_etHobbies = findViewById(R.id.activity_add_candidate_etHobbies);
        activity_add_candidate_etEmail = findViewById(R.id.activity_add_candidate_etEmail);

        //RadioButton

        activity_add_candidate_rbMale = findViewById(R.id.activity_add_candidate_rbMale);
        activity_add_candidate_rbFemale = findViewById(R.id.activity_add_candidate_rbFemale);

        //spinner

        activity_add_candidate_spLanguage = findViewById(R.id.activity_add_candidate_spLanguage);
        activity_add_candidate_spCity = findViewById(R.id.activity_add_candidate_spCity);

        //Button

        activity_add_candidate_btnSubmit = findViewById(R.id.activity_add_candidate_btnSubmit);

        //candidate =model initialize empty
        candidateModel = new CandidateModel();

        // Tbl CAndidate Object intialize
        tblCandidate = new TblCandidate(this);

        //Tbl City Initialize
        tblCity = new TblCity(this);

        //Tbl Language Initialize
        tblLanguage = new TblLanguage(this);

        //Language List Blank
        languageList = new ArrayList<>();

        //City List Empty Create
        cityList = new ArrayList<>();

        //Get Data From Database
        cityList = tblCity.all_city_ist();
        languageList = tblLanguage.all_language_ist();

        calendar = Calendar.getInstance();

        //Default Set Date
        calendar.set(Calendar.MONTH,0);
        calendar.set(Calendar.YEAR,2001);
        calendar.set(Calendar.DATE,1);

        //DatePicker Open And Update Data
        date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                // TODO Auto-generated method stub
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                AddCandidateActivity.this.updateLabel();
            }
        };
    }

    void updateLabel() {

        String dateFormat = "dd-MMM-yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.US);

        activity_add_candidate_etDOB.setText(sdf.format(calendar.getTime()));


    }

    private void process() {

        //Default focuse on name fild
        activity_add_candidate_etName.requestFocus();

        spCityAdapter = new SpCityAdapter(cityList,this);
        activity_add_candidate_spCity.setAdapter(spCityAdapter);

        spLanguageAdapter = new SpLanguageAdapter(languageList,this);
        activity_add_candidate_spLanguage.setAdapter(spLanguageAdapter);

    }


    private void listeners() {

        activity_add_candidate_etDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(AddCandidateActivity.this, date, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        activity_add_candidate_spLanguage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spLanguagePostion = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        activity_add_candidate_spCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spCityPostion = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        activity_add_candidate_btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (IsvalidDate()) {


                    //data set into tamp variable
                    getDateFormFields();


                    //Tamp data set into model
                    candidateModel.setName(name);
                    candidateModel.setFatherName(fatherName);
                    candidateModel.setSurNmae(surName);
                    candidateModel.setDOB(DOB);
                    candidateModel.setGender(1);
                    candidateModel.setMobile(mobile);
                    candidateModel.setEmail(email);
                    candidateModel.setHobbies(hobbies);
                    candidateModel.setCityId(cityList.get(spCityPostion).getCityId());
                    candidateModel.setLanguageId(languageList.get(spLanguagePostion).getLanguageId());
                    candidateModel.setIsFavorite(0);


                    //get data base pass

                    insertedCheck = tblCandidate.insertCandidateRecord(candidateModel);


                    //check inserted or not
                    if (insertedCheck > 0) {
                        Toast.makeText(AddCandidateActivity.this, "Inserted Sucsessfully", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(AddCandidateActivity.this, "Somthing went wormg", Toast.LENGTH_SHORT).show();
                    }


                }
            }
        });

    }

    public boolean IsvalidDate() {
        boolean valid = true;

        getDateFormFields();

        //name is not enpty
        if (TextUtils.isEmpty(name)) {

            activity_add_candidate_etName.setError("Please Enter some Detailes");
            activity_add_candidate_etName.requestFocus();
            valid = false;
        }

        // Name is not less then 3

        if (name.length() < 3) {

            activity_add_candidate_etName.setError("Please Enter some Detailes");
            activity_add_candidate_etName.requestFocus();
            valid = false;
        }

        //fathername is not enpty
        if (TextUtils.isEmpty(fatherName)) {

            activity_add_candidate_etFatherName.setError("Please Enter some Detailes");
            activity_add_candidate_etFatherName.requestFocus();
            valid = false;
        }

        // Father name is not less then 3
        // Name is not less then 3

        if (fatherName.length() < 3) {

            activity_add_candidate_etFatherName.setError("Please Enter some Detailes");
            activity_add_candidate_etFatherName.requestFocus();
            valid = false;
        }


        //  surname is not enpty
        if (TextUtils.isEmpty(surName)) {

            activity_add_candidate_etSurName.setError("Please Enter some Detailes");
            activity_add_candidate_etSurName.requestFocus();
            valid = false;
        }

        //surname is not less then 3
        // Name is not less then 3

        if (surName.length() < 3) {

            activity_add_candidate_etSurName.setError("Please Enter some Detailes");
            activity_add_candidate_etSurName.requestFocus();
            valid = false;
        }


        //email is not enpty

        if (TextUtils.isEmpty(email)) {

            activity_add_candidate_etEmail.setError("Please Enter some Detailes");
            activity_add_candidate_etEmail.requestFocus();
            valid = false;
        }


        //mobile is not enpty
        if (TextUtils.isEmpty(mobile)) {

            activity_add_candidate_etMObile.setError("Please Enter some Detailes");
            activity_add_candidate_etMObile.requestFocus();
            valid = false;
        }

        //mobile number is less then 10

        if (mobile.length() != 10) {

            activity_add_candidate_etMObile.setError("Please Enter some Detailes");
            activity_add_candidate_etMObile.requestFocus();
            valid = false;
        }

        //hobbiees is not enpty
        if (TextUtils.isEmpty(hobbies)) {

            activity_add_candidate_etHobbies.setError("Please Enter some Detailes");
            activity_add_candidate_etHobbies.requestFocus();
            valid = false;
        }

        //Dob is not enpty
        if (TextUtils.isEmpty(DOB)) {

            activity_add_candidate_etDOB.setError("Please Enter some Detailes");
            activity_add_candidate_etDOB.requestFocus();
            valid = false;
        }


        return valid;
    }


    void getDateFormFields() {

        // gUSTY GET DATA And set in tamp variable

        //EditText
        name = dataSpacesRemove(activity_add_candidate_etName.getText().toString());
        fatherName = dataSpacesRemove(activity_add_candidate_etFatherName.getText().toString());
        surName = dataSpacesRemove(activity_add_candidate_etSurName.getText().toString());
        email = dataSpacesRemove(activity_add_candidate_etEmail.getText().toString());
        mobile = dataSpacesRemove(activity_add_candidate_etMObile.getText().toString());
        hobbies = dataSpacesRemove(activity_add_candidate_etHobbies.getText().toString());
        DOB = dataSpacesRemove(activity_add_candidate_etDOB.getText().toString());

        //Radio Button

        rbMale = activity_add_candidate_rbMale.isChecked();


    }


    String dataSpacesRemove(String s) {
        return s.trim().toString();

    }


}
