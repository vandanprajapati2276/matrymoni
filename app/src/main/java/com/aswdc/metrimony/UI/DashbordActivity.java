package com.aswdc.metrimony.UI;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aswdc.metrimony.Database.TblCandidate;
import com.aswdc.metrimony.Model.CandidateModel;
import com.aswdc.metrimony.R;

import java.util.ArrayList;

public class DashbordActivity extends AppCompatActivity {

    //TextView going into new Activity
    TextView  activity_dashbord_tvAddCandidate,activity_dashbord_tvListCandidate,activity_dashbord_tvFavoriteCandidate,activity_dashbord_tvSearchCandidate;


    //TableCandidate Record Get
    TblCandidate tblCandidate;

    //ArrayList
    ArrayList<CandidateModel> candidateList;

    // intent Object for open new activity

    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashbord);


        init();
        process();
        listens();





    }
    // initialize of all Variables and Create of Null Object

    private void listens() {

        // AddActivity Onclick

        activity_dashbord_tvAddCandidate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(DashbordActivity.this,AddCandidateActivity.class);
                startActivity(intent);

            }
        });



        // ListActivity  On =click
        activity_dashbord_tvListCandidate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                intent = new Intent(DashbordActivity.this,ListActivity.class);
                startActivity(intent);
            }
        });

        // Favorit Oncilck

        activity_dashbord_tvFavoriteCandidate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                intent = new Intent(DashbordActivity.this,FavoriteActivity.class);
                startActivity(intent);
            }
        });

        // Search Onclick

        activity_dashbord_tvSearchCandidate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                intent = new Intent(DashbordActivity.this,SearchActivity.class);
                startActivity(intent);
            }
        });


    }

    private void process() {
    }

    private void init() {



        tblCandidate = new TblCandidate(this);
        candidateList = tblCandidate.all_CandidateList();



        //TextView Bind with ID
        activity_dashbord_tvAddCandidate = findViewById(R.id.activity_dashbord_tvAddCandidate);
        activity_dashbord_tvListCandidate = findViewById(R.id.activity_dashbord_tvListCandidate);
        activity_dashbord_tvFavoriteCandidate = findViewById(R.id.activity_dashbord_tvFavoriteCandidate);
        activity_dashbord_tvSearchCandidate = findViewById(R.id.activity_dashbord_tvSearchCandidate);


    }
}