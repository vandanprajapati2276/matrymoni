package com.aswdc.metrimony.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.aswdc.metrimony.Adapter.CandidateListAdapter;
import com.aswdc.metrimony.Database.TblCandidate;
import com.aswdc.metrimony.Model.CandidateModel;
import com.aswdc.metrimony.R;

import java.util.ArrayList;

public class ListActivity extends AppCompatActivity {

    //Array list
    ArrayList<CandidateModel> candidateList ;

    //Tblcandidate Object
    TblCandidate tblCandidate;

    //Candidate List Adepter Object
    CandidateListAdapter adapter;

    //Recycler View Object for display candidate list
    RecyclerView activity_list_candidate_rvCandidateList;

    //Textview object
    TextView activity_list_candidate_tvNODataFound;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_candidate);


        init();
        process();
        listeners();

    }

    //Initialize of  Object
    private void init() {

        //Array List Empty
        candidateList = new ArrayList<>();

        //Tblcandidate Initialize
        tblCandidate = new TblCandidate(this);

        //get data form database and send into array list
        candidateList = tblCandidate.all_CandidateList();

        //Recycler View Initialize
        activity_list_candidate_rvCandidateList = findViewById(R.id.activity_list_candidate_rvCandidateList);

        //TextView Initialize
        activity_list_candidate_tvNODataFound = findViewById(R.id.activity_list_candidate_tvNODataFound);


    }

    private void process() {

        if (candidateList.size()<1){

            activity_list_candidate_tvNODataFound.setVisibility(View.VISIBLE);
            activity_list_candidate_rvCandidateList.setVisibility(View.GONE);
        }else {

            activity_list_candidate_tvNODataFound.setVisibility(View.GONE);
            activity_list_candidate_rvCandidateList.setVisibility(View.VISIBLE);


              //Set Layout maneger for recuycler view
            activity_list_candidate_rvCandidateList.setLayoutManager(new GridLayoutManager(this ,1));

            //Adepter set
            adapter = new CandidateListAdapter(candidateList,this );

            //recycler View Adepter
            activity_list_candidate_rvCandidateList.setAdapter(adapter);

        }

    }

    private void listeners() {
    }
}