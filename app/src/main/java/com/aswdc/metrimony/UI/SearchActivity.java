package com.aswdc.metrimony.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.aswdc.metrimony.Adapter.CandidateListAdapter;
import com.aswdc.metrimony.Database.TblCandidate;
import com.aswdc.metrimony.Model.CandidateModel;
import com.aswdc.metrimony.R;

import java.util.ArrayList;

public class SearchActivity extends AppCompatActivity {

    //ArrayList Of Candidate
    ArrayList<CandidateModel> candidateList;

    //TblCanidate Object
    TblCandidate tblCandidate ;

    //Candidate List Adapter
    CandidateListAdapter adapter;


    //EditText Object
    EditText activity_search_etSearch;

    //TextView
    TextView activity_search_tvNoDataFound;


    //Recycler View Object
    RecyclerView activity_search_rvSearchList;


    String data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        init();
        process();
        listener();

    }

    private void listener() {

        activity_search_etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                data = s.toString();
                getDataFromDatabase();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    //RecyclerView Set Reset
    void setAdapter(){


        //Recycler View Layout Manager set
        activity_search_rvSearchList.setLayoutManager(new GridLayoutManager(this,1));

        //Adepter Set
        adapter = new CandidateListAdapter(candidateList,this);

        //Recycker View set Adapter
        activity_search_rvSearchList.setAdapter(adapter);

    }

    private void getDataFromDatabase(){

        //Array DAta get From DAtabase
          candidateList = tblCandidate.all_searchList(data) ;

        checkDataIsEmpty();
    }

    private void init() {

        //Edite Text Initialize
        activity_search_etSearch = findViewById(R.id.activity_search_etSearch);

         //Recycler View Initialize
        activity_search_rvSearchList = findViewById(R.id.activity_search_rvSearchList);


        //Candidate Array List Empty
        candidateList = new ArrayList<>();

        //TblCandidate  Initialize
        tblCandidate = new TblCandidate(this);

        //get data from database and set into Array list
        candidateList = tblCandidate.all_CandidateList();

        //TextView Initialize
        activity_search_tvNoDataFound = findViewById(R.id.activity_search_tvNoDataFound);

    }

    private void process() {

        checkDataIsEmpty();

    }


    void checkDataIsEmpty(){


        if (candidateList.size()<1){
            activity_search_tvNoDataFound.setVisibility(View.VISIBLE);
            activity_search_rvSearchList.setVisibility(View.GONE);
        }else {


            activity_search_tvNoDataFound.setVisibility(View.GONE);
            activity_search_rvSearchList.setVisibility(View.VISIBLE);

            setAdapter();

        }

        }


    }