package com.aswdc.metrimony.Database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.aswdc.metrimony.Model.LanguageModel;

import java.util.ArrayList;

public class TblLanguage extends DatabaseHelper {

    //table name
    final static String TBL_LANGUAGE ="TblLanguage";


    //column name
    final static String COL_TBL_LANGUAGE_LANGUAGEID ="LanguageId";
    final static String COL_TBL_LANGUAGE_LANGUAGENAME ="LanguageName";

    //GET DATA FORM DATABASE
    public ArrayList<LanguageModel> all_language_ist(){

      //sqlite database object
        SQLiteDatabase db = getReadableDatabase();

        //Create Arrey List Empty
        ArrayList<LanguageModel> languageList = new ArrayList<>( );

        //Sql Statement
        String  query = "select * from "+ TBL_LANGUAGE;




        // get ddata form database into Cursor
        //db.rawquery for select into database
        Cursor cursor = db.rawQuery(query,null);

        //loop for Cursor = tart to end the all data

        //cursor reset our potion
        cursor.moveToFirst();

        //first to last loop
        while (cursor.moveToNext()){

            //language model empty
            LanguageModel languageModel = new LanguageModel();

            //set language model
            languageModel.setLanguageId(cursor.getInt(cursor.getColumnIndex(COL_TBL_LANGUAGE_LANGUAGEID)));
            languageModel.setLanguageName(cursor.getString(cursor.getColumnIndex(COL_TBL_LANGUAGE_LANGUAGENAME)));

            //add model into array list
            languageList.add(languageModel);



        }


        //database close
        db.close();

        //cursor close
        cursor.close();

        //Return language list
        return languageList;


    }







    public TblLanguage(Context context) {
        super(context);
    }
}
