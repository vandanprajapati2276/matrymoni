package com.aswdc.metrimony.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import androidx.appcompat.view.menu.MenuBuilder;

import com.aswdc.metrimony.Model.CandidateModel;

import java.util.ArrayList;

public class TblCandidate extends DatabaseHelper {

    //tbl Name
    public static final String TBL_CANDIDATE = "TblCandidate";

    //Column Name
    public static final String COL_TBL_CANDIDATE_CANDIDATEID = "CandidateId";
    public static final String COL_TBL_CANDIDATE_NAME = "Name";
    public static final String COL_TBL_CANDIDATE_FATHERNAME = "FatherName";
    public static final String COL_TBL_CANDIDATE_SURNAME = "SurName";
    public static final String COL_TBL_CANDIDATE_EMAIL = "Email";
    public static final String COL_TBL_CANDIDATE_GENDER = "Gender";
    public static final String COL_TBL_CANDIDATE_CITYID = "CityId";
    public static final String COL_TBL_CANDIDATE_LANGUAGEID = "LanguageId";
    public static final String COL_TBL_CANDIDATE_MOBILE = "Mobile";
    public static final String COL_TBL_CANDIDATE_HOBBIES = "Hobbies";
    public static final String COL_TBL_CANDIDATE_DOB = "DOB";
    public static final String COL_TBL_CANDIDATE_ISFAVOITE = "IsFavorite";

    //select for db.rawQuery
    //update for db.update
    //insert for db.insert
    //delete for db.delete


    public TblCandidate(Context context) {

        super(context);

    }

    //insert into Database
    public long insertCandidateRecord(CandidateModel candidateModel) {

        //insrt/update/modify/delete that use getReadableDatabase*()
        //select / view  that use getWritebleDatabase()


        //sqlite datebase object
        SQLiteDatabase db = getWritableDatabase();

        //Content values blank
        ContentValues cv = new ContentValues();

        cv.put(COL_TBL_CANDIDATE_NAME, candidateModel.getName());
        cv.put(COL_TBL_CANDIDATE_FATHERNAME, candidateModel.getFatherName());
        cv.put(COL_TBL_CANDIDATE_SURNAME, candidateModel.getSurNmae());
        cv.put(COL_TBL_CANDIDATE_EMAIL, candidateModel.getEmail());
        cv.put(COL_TBL_CANDIDATE_MOBILE, candidateModel.getMobile());
        cv.put(COL_TBL_CANDIDATE_GENDER, candidateModel.getGender());
        cv.put(COL_TBL_CANDIDATE_HOBBIES, candidateModel.getHobbies());
        cv.put(COL_TBL_CANDIDATE_CITYID, candidateModel.getCityId());
        cv.put(COL_TBL_CANDIDATE_LANGUAGEID, candidateModel.getLanguageId());
        cv.put(COL_TBL_CANDIDATE_ISFAVOITE, candidateModel.getIsFavorite());
        cv.put(COL_TBL_CANDIDATE_DOB, candidateModel.getDOB());


        //insert Record into database
        long inserted = db.insert(TBL_CANDIDATE, null, cv);

        long update = db.update(TBL_CANDIDATE, cv, "" + COL_TBL_CANDIDATE_CANDIDATEID + "=?", new String[]{String.valueOf(candidateModel.getCandidateId())});

        //if record inserted  1 otherwise 0 or less

        return inserted;

    }


    //get Data From Databse
    public ArrayList<CandidateModel> all_CandidateList() {
        //Sqlite Object
        SQLiteDatabase db = getReadableDatabase();

        //ArrayList Of Candidate Model
        ArrayList<CandidateModel> candidateList = new ArrayList<>();

        //Query Statement
        String query = "SELECT * FROM " + TBL_CANDIDATE;

        //Cursor Object
        Cursor cursor = db.rawQuery(query, null);

        //Loop Of One By One Data Get And Insert Into List
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {

            do {
                CandidateModel candidateModel = returnCandidateModelFromCursor(cursor);
                candidateList.add(candidateModel);
            } while (cursor.moveToNext());
        }


        //Database Close
        db.close();
        //Cursor Close
        cursor.close();

        return candidateList;
    }


    public CandidateModel returnCandidateModelFromCursor(Cursor cursor) {

        //Create Empty CandidateModel
        CandidateModel candidateModel = new CandidateModel();

        candidateModel.setCandidateId(cursor.getInt(cursor.getColumnIndex(COL_TBL_CANDIDATE_CANDIDATEID)));
        candidateModel.setName(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_NAME)));
        candidateModel.setFatherName(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_FATHERNAME)));
        candidateModel.setSurNmae(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_SURNAME)));
        candidateModel.setLanguageId(cursor.getInt(cursor.getColumnIndex(COL_TBL_CANDIDATE_LANGUAGEID)));
        candidateModel.setCityId(cursor.getInt(cursor.getColumnIndex(COL_TBL_CANDIDATE_CITYID)));
        candidateModel.setHobbies(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_HOBBIES)));
        candidateModel.setEmail(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_EMAIL)));
        candidateModel.setMobile(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_MOBILE)));
        candidateModel.setDOB(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_DOB)));
        candidateModel.setGender(cursor.getInt(cursor.getColumnIndex(COL_TBL_CANDIDATE_GENDER)));
        candidateModel.setIsFavorite(cursor.getInt(cursor.getColumnIndex(COL_TBL_CANDIDATE_ISFAVOITE)));


        //Retrun Model
        return candidateModel;

    }

    //chang IsfavoriteValue into TblCadidte Table
    public void changeFavorite(int candidateId, int favoriteValue) {

        //SQLite database object
        SQLiteDatabase db = getWritableDatabase();

        //SQL Stenment
        String query = "Update " + TBL_CANDIDATE + " SET " + COL_TBL_CANDIDATE_ISFAVOITE + " = " + favoriteValue + " where " + COL_TBL_CANDIDATE_CANDIDATEID + " = " + candidateId;


        //Run Sql query
        db.execSQL(query);

        //database colse
        db.close();


    }

    //Delete candidate Details
    public void deleteCandidateRecord(int candidateId) {

        //SQLite Object
        SQLiteDatabase db = getWritableDatabase();

        //SQL Statemnt
        String query = "delete from " + TBL_CANDIDATE + " where " + COL_TBL_CANDIDATE_CANDIDATEID + " = " + candidateId;

        //execute query
        db.execSQL(query);

        //closed database
        db.close();


    }


    //Select TblcCandidate Table for FavoriteRecords
    public ArrayList<CandidateModel> all_CandidateFavoriteList() {
        //Sqlite Object
        SQLiteDatabase db = getReadableDatabase();

        //ArrayList Of Candidate Model
        ArrayList<CandidateModel> candidateList = new ArrayList<>();

        //Query Statement
        String query = "SELECT * FROM " + TBL_CANDIDATE+ " where "+COL_TBL_CANDIDATE_ISFAVOITE+" =1";

        //Cursor Object
        Cursor cursor = db.rawQuery(query, null);

        //Loop Of One By One Data Get And Insert Into List
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {

            do {
                CandidateModel candidateModel = returnCandidateModelFromCursor(cursor);
                candidateList.add(candidateModel);
            } while (cursor.moveToNext());
        }


        //Database Close
        db.close();
        //Cursor Close
        cursor.close();

        return candidateList;
    }



    //get Data From Database
    public ArrayList<CandidateModel> all_searchList(String data) {
        //Sqlite Object
        SQLiteDatabase db = getReadableDatabase();

        //ArrayList Of Candidate Model
        ArrayList<CandidateModel> candidateList = new ArrayList<>();

        //Query Statement
        String query = "SELECT * FROM " + TBL_CANDIDATE+" where "+COL_TBL_CANDIDATE_NAME+" LIKE '%"+data+"%'";

        //Cursor Object
        Cursor cursor = db.rawQuery(query, null);

        //Loop Of One By One Data Get And Insert Into List
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {

            do {
                CandidateModel candidateModel = returnCandidateModelFromCursor(cursor);
                candidateList.add(candidateModel);
            } while (cursor.moveToNext());
        }


        //Database Close
        db.close();
        //Cursor Close
        cursor.close();

        return candidateList;
    }








}
