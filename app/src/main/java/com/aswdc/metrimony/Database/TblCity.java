package com.aswdc.metrimony.Database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.aswdc.metrimony.Model.CityModel;
import com.aswdc.metrimony.Model.LanguageModel;

import java.util.ArrayList;

import static com.aswdc.metrimony.Database.TblLanguage.TBL_LANGUAGE;

public class TblCity extends DatabaseHelper {



    //table name
    final static String TBL_CITY ="TblCity";


    //column name
    final static String COL_TBL_CITY_CITYID ="CityId";
    final static String COL_TBL_CITY_CITYNAME ="CityName";

    //GET DATA FORM DATABASE
    public ArrayList<CityModel> all_city_ist(){

        //sqlite database object
        SQLiteDatabase db = getReadableDatabase();

        //Create Arrey List Empty
        ArrayList<CityModel> cityList = new ArrayList<>( );

        //Sql Statement
        String  query = "select * from "+ TBL_CITY;




        // get ddata form database into Cursor
        //db.rawquery for select into database
        Cursor cursor = db.rawQuery(query,null);

        //loop for Cursor = tart to end the all data

        //cursor reset our potion
        cursor.moveToFirst();

        //first to last loop
        while (cursor.moveToNext()){

            //language model empty
            CityModel cityModel = new CityModel();

            //set language model
            cityModel.setCityId(cursor.getInt(cursor.getColumnIndex(COL_TBL_CITY_CITYID)));
            cityModel.setCityName(cursor.getString(cursor.getColumnIndex(COL_TBL_CITY_CITYNAME)));


            //add model into array list
            cityList.add(cityModel);



        }


        //database close
        db.close();

        //cursor close
        cursor.close();

        //Return city list
        return cityList;


    }






    public TblCity(Context context) {
        super(context);
    }
}
