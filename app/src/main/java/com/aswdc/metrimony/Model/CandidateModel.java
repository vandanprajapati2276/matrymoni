package com.aswdc.metrimony.Model;

import java.io.Serializable;

public class CandidateModel implements Serializable {

    String Name;
    String SurNmae;
    String FatherName;
    String Email;
    String Mobile;
    String Hobbies;
    String DOB;
    int CandidateId;
    int IsFavorite;
    int Gender;
    int CityId;
    int languageId;


    public int getCandidateId() {
        return CandidateId;
    }

    public void setCandidateId(int candidateId) {
        CandidateId = candidateId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSurNmae() {
        return SurNmae;
    }

    public void setSurNmae(String surNmae) {
        SurNmae = surNmae;
    }

    public String getFatherName() {
        return FatherName;
    }

    public void setFatherName(String fatherName) {
        FatherName = fatherName;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String mobile) {
        Mobile = mobile;
    }

    public String getHobbies() {
        return Hobbies;
    }

    public void setHobbies(String hobbies) {
        Hobbies = hobbies;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public int getIsFavorite() {
        return IsFavorite;
    }

    public void setIsFavorite(int isFavorite) {
        IsFavorite = isFavorite;
    }

    public int getGender() {
        return Gender;
    }

    public void setGender(int gender) {
        Gender = gender;
    }

    public int getCityId() {
        return CityId;
    }

    public void setCityId(int cityId) {
        CityId = cityId;
    }

    public int getLanguageId() {
        return languageId;
    }

    public void setLanguageId(int languageId) {
        this.languageId = languageId;
    }







}
