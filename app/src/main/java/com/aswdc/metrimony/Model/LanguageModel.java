package com.aswdc.metrimony.Model;

import java.io.Serializable;

public class LanguageModel implements Serializable {

   int LanguageId;
   String LanguageName;

    public int getLanguageId() {
        return LanguageId;
    }

    public void setLanguageId(int languageId) {
        LanguageId = languageId;
    }

    public String getLanguageName() {
        return LanguageName;
    }

    public void setLanguageName(String languageName) {
        LanguageName = languageName;
    }

    @Override
    public String toString() {
        return "LanguageModel{" +
                "LanguageId=" + LanguageId +
                ", LanguageName='" + LanguageName + '\'' +
                '}';
    }
}
