package com.aswdc.metrimony.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.aswdc.metrimony.Model.CityModel;
import com.aswdc.metrimony.R;

import java.util.ArrayList;

public class SpCityAdapter extends BaseAdapter {

    //ArrayList Object Of CityModel
    ArrayList<CityModel> cityList;
    Activity context;

    //Holder Object
    ViewHolder holder;

    public SpCityAdapter(ArrayList<CityModel> cityList, Activity context) {
        this.cityList = cityList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return cityList.size();
    }

    @Override
    public Object getItem(int position) {
        return cityList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return cityList.get(position).getCityId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater layoutInflater = context.getLayoutInflater();

        if(convertView == null){

            convertView = layoutInflater.inflate(R.layout.row_candidate_city,parent,false);

            holder = new SpCityAdapter.ViewHolder();

            holder.row_candidate_city_tvCityId = convertView.findViewById(R.id.row_candidate_city_tvCityId);
            holder.row_candidate_city_tvCityName = convertView.findViewById(R.id.row_candidate_city_tvCityName);

            convertView.setTag(holder);

        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.row_candidate_city_tvCityId.setText(String.valueOf(cityList.get(position).getCityId()));
        holder.row_candidate_city_tvCityName.setText(cityList.get(position).getCityName());

        return convertView;
    }

    class ViewHolder{
        //TextView
        TextView row_candidate_city_tvCityName,row_candidate_city_tvCityId;
    }
}
