package com.aswdc.metrimony.Adapter;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aswdc.metrimony.Database.TblCandidate;
import com.aswdc.metrimony.Model.CandidateModel;
import com.aswdc.metrimony.R;

import java.util.ArrayList;

public class CandidateListAdapter extends RecyclerView.Adapter<CandidateListAdapter.ViewHolder> {

    //Array List of Candidate
    ArrayList<CandidateModel> candidateList;

    //Context of activity
    Context context;

    //TblCandidate Object
    TblCandidate tblCandidate;

    public CandidateListAdapter(ArrayList<CandidateModel> candidateList, Context context) {
        this.candidateList = candidateList;
        this.context = context;
        tblCandidate = new TblCandidate(context);
    }

    @NonNull
    @Override
    public CandidateListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        //Attach Row file with ViewHolder
       View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_candidate_list,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CandidateListAdapter.ViewHolder holder, final int position) {

        if (candidateList.size()<1){
            holder.activity_list_candidate_tvNODataFound.setVisibility(View.VISIBLE);

        }


        //Display Name , Email, Dob
        holder.row_candidate_list_tvName.setText(candidateList.get(position).getName());
        holder.row_candidate_list_tvEmail.setText(candidateList.get(position).getEmail());
        holder.row_candidate_list_tvDOB.setText(candidateList.get(position).getDOB());

        if(candidateList.get(position).getIsFavorite() == 0){
            //Is Favorite is 0 then uunfavorite data
            holder.row_candidate_list_ivFavoriteCandidate.setImageResource(R.drawable.ic_unfavorite);
        }else{
            //Is favorite 1 then fat=vorite data
            holder.row_candidate_list_ivFavoriteCandidate.setImageResource(R.drawable.baseline_favorite_black_48);
        }

        //Favorite Click
        holder.row_candidate_list_ivFavoriteCandidate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TblCandidate favorite value chang
                tblCandidate.changeFavorite(candidateList.get(position).getCandidateId(),candidateList.get(position).getIsFavorite() == 0 ? 1 : 0);

                // candiadte list  isfavorite Data change
                candidateList.get(position).setIsFavorite(candidateList.get(position).getIsFavorite() == 0 ? 1 : 0);

                //Aeter rest
                notifyDataSetChanged();
            }
        });


        //Delete Click
        holder.row_candidate_list_ivDeleteCandidate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Tblcandidate reomve candidate details
                tblCandidate.deleteCandidateRecord(candidateList.get(position).getCandidateId());

                ////remove form list
                candidateList.remove(candidateList.get(position));

                //Adepter Reset
                notifyItemRemoved(position);
                notifyDataSetChanged();
            }
        });


    }

    @Override
    public int getItemCount() {
        return candidateList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        //Bind all Object with id


        //TextView Object
        TextView row_candidate_list_tvName,row_candidate_list_tvEmail,row_candidate_list_tvDOB,activity_list_candidate_tvNODataFound;

        //ImageView Object
        ImageView row_candidate_list_ivFavoriteCandidate,row_candidate_list_ivDeleteCandidate;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            //TextView Initialize
            row_candidate_list_tvName = itemView.findViewById(R.id.row_candidate_list_tvName);
            row_candidate_list_tvEmail = itemView.findViewById(R.id.row_candidate_list_tvEmail);
            row_candidate_list_tvDOB = itemView.findViewById(R.id.row_candidate_list_tvDOB);
            activity_list_candidate_tvNODataFound = itemView.findViewById(R.id.activity_list_candidate_tvNODataFound);

            //ImageView Initialize
            row_candidate_list_ivFavoriteCandidate = itemView.findViewById(R.id.row_candidate_list_ivFavoriteCandidate);
            row_candidate_list_ivDeleteCandidate = itemView.findViewById(R.id.row_candidate_list_ivDeleteCandidate);

        }
    }
}
