package com.aswdc.metrimony.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.aswdc.metrimony.Model.LanguageModel;
import com.aswdc.metrimony.R;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class SpLanguageAdapter extends BaseAdapter {

    //Arraylist
    ArrayList<LanguageModel> languageList;
    Activity context;

    //ViewHolder Object
    ViewHolder holder;

    public SpLanguageAdapter(ArrayList<LanguageModel> languageList, Activity context) {
        this.languageList = languageList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return languageList.size();
    }

    @Override
    public Object getItem(int position) {
        return languageList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return languageList.get(position).getLanguageId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater layoutInflater = context.getLayoutInflater();

        if(convertView == null){

            convertView = layoutInflater.inflate(R.layout.row_candidate_language,parent,false);

            holder = new SpLanguageAdapter.ViewHolder();

            holder.row_candidate_language_tvLanguageId = convertView.findViewById(R.id.row_candidate_language_tvLanguageId);
            holder.row_candidate_language_tvLanguageName = convertView.findViewById(R.id.row_candidate_language_tvLanguageName);

            convertView.setTag(holder);

        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.row_candidate_language_tvLanguageId.setText(String.valueOf(languageList.get(position).getLanguageId()));
        holder.row_candidate_language_tvLanguageName.setText(languageList.get(position).getLanguageName());

        return convertView;
    }

    class ViewHolder{
        //TextView

        TextView row_candidate_language_tvLanguageId,row_candidate_language_tvLanguageName;

    }
}
